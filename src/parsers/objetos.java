package parsers;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Luis Salazar
 */
public class objetos {
    String Nombre, Ruta, Tipo;
    int Puntos,Crecimiento,Tiempo;

    public objetos(String Nombre, String Ruta, String Tipo, int Puntos,int Crecimiento, int Tiempo){
        Nombre = this.Nombre;
        Ruta = this.Ruta;
        Tipo = this.Tipo;
        Puntos = this.Puntos;
        Crecimiento= this.Crecimiento;
        Tiempo= this.Tiempo;

    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getRuta() {
        return Ruta;
    }

    public void setRuta(String ruta) {
        Ruta = ruta;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    public int getPuntos() {
        return Puntos;
    }

    public void setPuntos(int puntos) {
        Puntos = puntos;
    }
    
    public int getCrecimiento() {
        return Crecimiento;
    }

    public void setCrecimiento(int crecer) {
        Crecimiento = crecer;
    }
    public int getTiempo() {
        return Tiempo;
    }

    public void setTiempo(int time) {
        Tiempo = time;
    }
}
