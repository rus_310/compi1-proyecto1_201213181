package parsers;
import java_cup.runtime.Symbol;

%%

%class lexico3
%cupsym tabla_simbolos3
%cup
%public
%unicode
%line
%char
%ignorecase

palabra =[a-zA-Z]+
nombrefo = {palabra}[_]?[0-9]*


%%
"estrategias"       {return new Symbol(tabla_simbolos3.estrategias, yychar,yyline); }
"Movimientos"       {return new Symbol(tabla_simbolos3.movimientos, yychar,yyline); }
"arriba"            {return new Symbol(tabla_simbolos3.arriba, yychar,yyline); }
"abajo"             {return new Symbol(tabla_simbolos3.abajo, yychar,yyline); }
"derecha"           {return new Symbol(tabla_simbolos3.derecha, yychar,yyline); }
"izquierda"         {return new Symbol(tabla_simbolos3.izquierda, yychar,yyline); }
"atras"             {return new Symbol(tabla_simbolos3.atras, yychar,yyline); }


":"                    {return new Symbol(tabla_simbolos3.dospuntos, yychar,yyline); }
"{"                     {return new Symbol(tabla_simbolos3.llavea, yychar,yyline); }
"["                     {return new Symbol(tabla_simbolos3.corchetea, yychar,yyline); }
","                     {return new Symbol(tabla_simbolos3.coma, yychar,yyline); }
"}"                     {return new Symbol(tabla_simbolos3.llavec, yychar,yyline); }
"]"                     {return new Symbol(tabla_simbolos3.corchetec, yychar,yyline); }

{nombrefo}            {return new Symbol(tabla_simbolos3.nombrefo, yychar,yyline, new String(yytext())); }

[ \t\r\f\n]+       { /* Se ignoran */}

.   { System.out.println("Error lexico: "+yytext()); }