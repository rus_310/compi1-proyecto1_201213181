/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package parsers;

/**
 *
 * @author Luis Salazar
 */
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Imagen extends javax.swing.JPanel {
    int x, y;
    principal d= new principal();
    private String rutafondo;

    public Imagen(JPanel jPanel1) {
        this.x = jPanel1.getWidth();
        this.y = jPanel1.getHeight();
        this.setSize(x, y);
    }

    @Override
    public void paint(Graphics g) {
       for(int i = 0; i< d.listafondos.size(); i++){
        if (d.nombrefondo.equals(d.listafondos.get(i).getNombre())){
            rutafondo = d.listafondos.get(i).getRuta();
            break;
        }}
        System.out.println(rutafondo);
        ImageIcon Img = new ImageIcon(rutafondo);
        
        g.drawImage(Img.getImage(), 0, 0, x, y, null);
    }    

}