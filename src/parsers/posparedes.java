/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package parsers;

/**
 *
 * @author Luis Salazar
 */
public class posparedes {

    String Nombre;
    boolean Horizontal;
    int Ini, Fin, Cte;

    public posparedes(String Nombre, boolean Horizontal, int Ini, int Fin ,int Cte){

        Nombre = this.Nombre;
        Horizontal = this.Horizontal;
        Ini = this.Ini;
        Fin = this.Fin;
        Cte = this.Cte;

    }

    public boolean eshorizontal() {
        return Horizontal;
    }

    public void setHorizontal(boolean horizontal) {
        Horizontal = horizontal;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public int getInicio() {
        return Ini;
    }

    public void setInicio(int inicio) {
        Ini = inicio;
    }

    public int getFin() {
        return Fin;
    }

    public void setFin(int finaliza) {
        Fin = finaliza;
    }

    public int getConstate() {
        return Cte;
    }

    public void setConstante(int constante) {
        Cte = constante;
    }

}
