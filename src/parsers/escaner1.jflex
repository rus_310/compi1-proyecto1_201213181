package parsers;
import java_cup.runtime.Symbol;
%%
%cupsym tabla_simbolos
%class lexico
%cup
%public
%unicode
%line
%column
%char
%ignorecase

numero =[0-9]+ "."? [0-9]*
palabra =[a-zA-Z]+

oracion = {palabra}({palabra}|{numero}| ":" | "_" | "\\" |"["|"]"| "." | ",")*

sstring = [\"]{oracion}[\"]

nombrefo = {palabra}[_]?[0-9]*

points= "-"{numero}


%%

    /* PALABRAS RESERVADAS */

    "<configuracion>"       {return new Symbol(tabla_simbolos.acon, yychar,yyline); }
    "<fondo>"            {return new Symbol(tabla_simbolos.afon, yychar,yyline);}
    "<disenio>"             {return new Symbol(tabla_simbolos.adis, yychar,yyline); }

    "</configuracion>"       {return new Symbol(tabla_simbolos.ccon, yychar,yyline); }
    "</fondo>"            {return new Symbol(tabla_simbolos.cfon, yychar,yyline);}
    "</disenio>"             {return new Symbol(tabla_simbolos.cdis, yychar,yyline); }

    /* PROPIEDADES */
    "nombre"                {return new Symbol(tabla_simbolos.nombre, yychar,yyline); }
    "path"                  {return new Symbol(tabla_simbolos.path, yychar,yyline); }
    "puntos"              {return new Symbol(tabla_simbolos.puntos, yychar,yyline); }
    "tipo"              {return new Symbol(tabla_simbolos.tipo, yychar,yyline); }
    "crecimiento"              {return new Symbol(tabla_simbolos.crecimiento, yychar,yyline); }
    "tiempo"              {return new Symbol(tabla_simbolos.tiempo, yychar,yyline); }
    "manzana"              {return new Symbol(tabla_simbolos.manzana, yychar,yyline); }
    "bloque"              {return new Symbol(tabla_simbolos.bloque, yychar,yyline); }   
    "bonus"              {return new Symbol(tabla_simbolos.bonus, yychar,yyline); }
    "bomba"              {return new Symbol(tabla_simbolos.bomba, yychar,yyline); }
    "estrella"              {return new Symbol(tabla_simbolos.estrella, yychar,yyline); } 
    "veneno"              {return new Symbol(tabla_simbolos.veneno, yychar,yyline); }

    /* SEPARADOR */
    "["                     {return new Symbol(tabla_simbolos.llavea, yychar,yyline); }
    "]"                     {return new Symbol(tabla_simbolos.llavec, yychar,yyline); }
    ","                     {return new Symbol(tabla_simbolos.coma, yychar,yyline); }
    "="                     {return new Symbol(tabla_simbolos.igual, yychar,yyline); }
    ";"                     {return new Symbol(tabla_simbolos.puntocoma, yychar,yyline); }

    {nombrefo}            {return new Symbol(tabla_simbolos.nombrefo, yychar,yyline, new String(yytext())); }
    {oracion}             {return new Symbol(tabla_simbolos.oracion, yychar,yyline, new String(yytext())); }
    {sstring}             {return new Symbol(tabla_simbolos.sstring, yychar, yyline, new String(yytext())); }
    {numero}              {return new Symbol(tabla_simbolos.numero, yychar, yyline, new String(yytext())); }
    {points}              {return new Symbol(tabla_simbolos.valor, yychar, yyline, new String(yytext())); }
   
    
/* BLANCOS */
[ \t\r\f\n]+ { /* Se ignoran */}

/* CUAQUIER OTRO */
. { return new Symbol(tabla_simbolos.errorlex, yycolumn,yyline, new String(yytext())); } 