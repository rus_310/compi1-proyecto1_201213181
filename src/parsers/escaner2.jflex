package parsers;
import java_cup.runtime.Symbol;

%%

%class lexico2
%cupsym tabla_simbolos2
%cup
%public
%unicode
%line
%char
%ignorecase

numero =[0-9]+
palabra =[a-zA-Z]+
oracion ={palabra}({palabra}|{numero}| ":" | " " | "_" | "\\" | ".")*
nombrefo = {palabra}[_]?[0-9]*


%%
"escenario fondo"       {return new Symbol(tabla_simbolos2.aescenario, yychar,yyline); }
"ancho"                 {return new Symbol(tabla_simbolos2.ancho, yychar,yyline); }
"alto"                  {return new Symbol(tabla_simbolos2.alto, yychar,yyline); }
"paredes"               {return new Symbol(tabla_simbolos2.aparedes, yychar,yyline); }
"extras"                {return new Symbol(tabla_simbolos2.aextras, yychar,yyline); }
"estrella"                 {return new Symbol(tabla_simbolos2.aestrella, yychar,yyline); }
"bonus"                 {return new Symbol(tabla_simbolos2.abonus, yychar,yyline); }
"manzana"                  {return new Symbol(tabla_simbolos2.amanzana, yychar,yyline); }
"veneno"                  {return new Symbol(tabla_simbolos2.aveneno, yychar,yyline); }
"bomba"                 {return new Symbol(tabla_simbolos2.abomba, yychar,yyline); }

"/escenario"             {return new Symbol(tabla_simbolos2.cescenario, yychar,yyline); }
"/paredes"               {return new Symbol(tabla_simbolos2.cparedes, yychar,yyline); }
"/extras"                {return new Symbol(tabla_simbolos2.cextras, yychar,yyline); }
"/estrella"              {return new Symbol(tabla_simbolos2.cestrella, yychar,yyline); }
"/bonus"                 {return new Symbol(tabla_simbolos2.cbonus, yychar,yyline); }
"/manzana"               {return new Symbol(tabla_simbolos2.cmanzana, yychar,yyline); }
"/veneno"                {return new Symbol(tabla_simbolos2.cveneno, yychar,yyline); }
"/bomba"                 {return new Symbol(tabla_simbolos2.cbomba, yychar,yyline); }

".."                    {return new Symbol(tabla_simbolos2.rango, yychar,yyline); }
"("                     {return new Symbol(tabla_simbolos2.parenta, yychar,yyline); }
")"                     {return new Symbol(tabla_simbolos2.parentc, yychar,yyline); }
","                     {return new Symbol(tabla_simbolos2.coma, yychar,yyline); }
";"                     {return new Symbol(tabla_simbolos2.puntocoma, yychar,yyline); }
"="                     {return new Symbol(tabla_simbolos2.igual, yychar,yyline); }
"<"                     {return new Symbol(tabla_simbolos2.etiqa, yychar,yyline); }
">"                     {return new Symbol(tabla_simbolos2.etiqc, yychar,yyline); }

{nombrefo}            {return new Symbol(tabla_simbolos2.nombrefo, yychar,yyline, new String(yytext())); }
{numero}           {return new Symbol(tabla_simbolos2.numero, yychar, yyline, new String(yytext())); }

[ \t\r\f\n]+       { /* Se ignoran */}

.   { System.out.println("Error lexico: "+yytext()); }