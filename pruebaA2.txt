<escenario fondo=Fondo_1; ancho=20; alto=20;>
	<paredes>
		suelo1(0..2,5); 
		suelo2(0,17);
		suelo2(17,10..15);
		suelo2(12..17,9);
		suelo2(11,18);
		suelo2(12..16,19);
	</paredes>
	<extras> 
		<bonus> 
			diamante1(14,13); 
		</bonus> 
		<estrella> 
			luz(11,13); 
		</estrella> 
	</extras> 
</escenario>